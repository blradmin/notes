### - Basic Commands -

Clone gitlab project to your local system.

`git clone http://183.82.107.231:9099/kaushik/python.git`

`touch 1.txt 2.txt 3.txt`

`git add filename`

`git commit -m "commenting the file"`

`git push -u origin master `

or

`git push -u origin branchname`

# Existing folder push to gitlab

`git init`

`git add existing_foldername`

`git commit -m "commenting existed folder"`

`git push origin master/branchname`

# Creating branch in local repository

`git branch branch_name`

or

`git checkout -b branch_name`

# Sending data to your new branch

`touch testing.txt`

`git add testing.txt`

`git commit -m "testing branch"`

`git push -u origin branch_name`


# Deleting branch in remote repository

Need to come out from the branch name you wanna delete

`git status`

`git checkout master`

`git push origin --delete branch_name`

# Deleting branch in local_repository

Need to come out from the branch name you wanna delete

`git status`

`git checkout master`

`git branch -D branchname`

# How to create empty branch on git

`git checkout --orphan NEWBRANCH`

`git rm -rf .`

`vim run.sh`

`git add run.sh`

`git commit -m "creating new branch and pushing data"`

`git push -u origina NEWBRANCH`

```
Note: when ever you create an branch, all your present data will be clone so you need to clear.
      Once you found, your are in created branch use git "rm -rf ." command.
```

# How to save username and password in Git

Run

`git config --global credential.helper store`

provide a username and password and those details will then be remembered later.

The credentials are stored in a file on the disk, with the disk permissions of "just user readable/writable" but still in plaintext.

# To Remove Git Credentials

Run the following command in the terminal to remove your credentials stored in the cache

`git config --global --unset credential.helper`
